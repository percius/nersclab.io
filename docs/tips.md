# Tips and Tricks

This page is for tips contributed by NERSC users, which may be moved or 
copied in time to appropriate locations elsewhere in this documentation.
If you have an addition to contribute to <http://docs.nersc.gov> but are
not sure where it should go, this is a good page to make a merge request 
to.

A guide for contributors is at 
<https://gitlab.com/NERSC/nersc.gitlab.io/blob/main/CONTRIBUTING.md>.
